﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DariShop.Entities
{
    public class History : BaseEntity
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public DateTime DateSold { get; set; }
        public int Count { get; set; }
        public int ProductPrice { get; set; }
        public string ProductName { get; set; }
    }
}
