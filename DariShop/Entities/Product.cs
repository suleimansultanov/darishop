﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DariShop.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public int MainCost { get; set; }
        public int Count { get; set; }
        public string ImagePath { get; set; }
        public List<ProductBasket> ProductBasket { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
