﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DariShop.Entities
{
    public class Sale : BaseEntity
    {
        public int MainProfit { get; set; }
        public int Profit { get; set; }
    }
}
