﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DariShop.Entities;
using Microsoft.AspNetCore.Identity;

namespace DariShop
{
    public static class IdentityInitializer
    {
        public static void SeedUsers
            (UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            try
            {
                if (roleManager.FindByNameAsync("owner").Result == null)
                {
                    IdentityRole role = new IdentityRole("owner");
                    var result = roleManager.CreateAsync(role).Result;
                }
                if (roleManager.FindByNameAsync("admin").Result == null)
                {
                    IdentityRole role = new IdentityRole("admin");
                    var result = roleManager.CreateAsync(role).Result;
                }
                if (roleManager.FindByNameAsync("user").Result == null)
                {
                    IdentityRole role = new IdentityRole("user");
                    var result = roleManager.CreateAsync(role).Result;
                }

                if (userManager.FindByNameAsync
                        ("owner@owner.com").Result == null)
                {
                    AppUser owner = new AppUser();
                    owner.UserName = "owner@owner.com";
                    owner.Email = "owner@owner.com";


                    IdentityResult result = userManager.CreateAsync
                        (owner, "Owner123@").Result;

                    if (!userManager.IsInRoleAsync(owner, "owner").Result)
                    {
                        var res = userManager.AddToRoleAsync(owner, "owner").Result;
                    }
                }

                if (userManager.FindByNameAsync
                    ("admin@admin.com").Result == null)
                {

                    AppUser admin = new AppUser();
                    admin.UserName = "admin@admin.com";
                    admin.Email = "admin@admin.com";


                    IdentityResult result2 = userManager.CreateAsync
                        (admin, "Admin123@").Result;

                    if (!userManager.IsInRoleAsync(admin, "admin").Result)
                    {
                        var res = userManager.AddToRoleAsync(admin, "admin").Result;
                    }
                }

                if (userManager.FindByNameAsync("user@user.com").Result == null)
                {
                    AppUser user = new AppUser();
                    user.UserName = "user@user.com";
                    user.Email = "user@user.com";


                    IdentityResult result3 = userManager.CreateAsync
                        (user, "User123@").Result;


                    if (!userManager.IsInRoleAsync(user, "user").Result)
                    {
                        var res = userManager.AddToRoleAsync(user, "user").Result;
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }
    }
}
