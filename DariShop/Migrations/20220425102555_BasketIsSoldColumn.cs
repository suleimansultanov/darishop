﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DariShop.Migrations
{
    public partial class BasketIsSoldColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSold",
                table: "ProductBasket",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSold",
                table: "ProductBasket");
        }
    }
}
