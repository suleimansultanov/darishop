﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DariShop.Migrations
{
    public partial class BasketList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProductBasket_ProductId",
                table: "ProductBasket");

            migrationBuilder.CreateIndex(
                name: "IX_ProductBasket_ProductId",
                table: "ProductBasket",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProductBasket_ProductId",
                table: "ProductBasket");

            migrationBuilder.CreateIndex(
                name: "IX_ProductBasket_ProductId",
                table: "ProductBasket",
                column: "ProductId",
                unique: true);
        }
    }
}
