﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DariShop.Models
{
    public class StatsViewModel
    {
        public DateTime DateTime { get; set; }
        public int Profit { get; set; }
    }
}
