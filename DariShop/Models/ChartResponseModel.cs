﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DariShop.Models
{
    public class ChartResponseModel
    {
        public List<Chart> numberOfOrders { get; set; }
        public int yearNumber { get; set; }
        public int monthNumber { get; set; }
    }

    public class Chart
    {
        public int SaleCount { get; set; }
        public int DayNumber { get; set; }
        public int MonthNumber { get; set; }
    }
}
