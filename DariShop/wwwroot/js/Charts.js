﻿$(document).ready(function () {

    //$("#sparkline1").sparkline([1, 3, 4, 65, 6, 1, 8, 1], {
    //    type: 'line',
    //    width: '100%',
    //    height: '50',
    //    lineColor: '#1ab394',
    //    fillColor: "transparent"
    //});
    var data2 = [];
    //var data3 = [
    //    [gd(2020, 1, 1), 7], [gd(2020, 1, 2), 6], [gd(2020, 1, 3), 4], [gd(2020, 1, 4), 8],
    //    [gd(2020, 1, 5), 9], [gd(2020, 1, 6), 7], [gd(2020, 1, 7), 5], [gd(2020, 1, 8), 4],
    //    [gd(2020, 1, 9), 7], [gd(2020, 1, 10), 8], [gd(2020, 1, 11), 9], [gd(2020, 1, 12), 6],
    //    [gd(2020, 1, 13), 4], [gd(2020, 1, 14), 5], [gd(2020, 1, 15), 11], [gd(2020, 1, 16), 8],
    //    [gd(2020, 1, 17), 8], [gd(2020, 1, 18), 11], [gd(2020, 1, 19), 11], [gd(2020, 1, 20), 6],
    //    [gd(2020, 1, 21), 6], [gd(2020, 1, 22), 8], [gd(2020, 1, 23), 11], [gd(2020, 1, 24), 13],
    //    [gd(2020, 1, 25), 7], [gd(2020, 1, 26), 9], [gd(2020, 1, 27), 9], [gd(2020, 1, 28), 8],
    //    [gd(2020, 1, 29), 5], [gd(2020, 1, 30), 8], [gd(2020, 1, 31), 25]
    //];

    $.ajax({
        type: 'GET',
        url: '?handler=GetInfo',
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log(data);

            var yearNumber = data.yearNumber;
            var monthNumber = data.monthNumber;

            data2 = [
                [gd(yearNumber, monthNumber, 1), data.numberOfOrders[0].saleCount], [gd(yearNumber, monthNumber, 2), data.numberOfOrders[1].saleCount], [gd(yearNumber, monthNumber, 3), data.numberOfOrders[2].saleCount], [gd(yearNumber, monthNumber, 4), data.numberOfOrders[3].saleCount],
                [gd(yearNumber, monthNumber, 5), data.numberOfOrders[4].saleCount], [gd(yearNumber, monthNumber, 6), data.numberOfOrders[5].saleCount], [gd(yearNumber, monthNumber, 7), data.numberOfOrders[6].saleCount], [gd(yearNumber, monthNumber, 8), data.numberOfOrders[7].saleCount],
                [gd(yearNumber, monthNumber, 9), data.numberOfOrders[8].saleCount], [gd(yearNumber, monthNumber, 10), data.numberOfOrders[9].saleCount], [gd(yearNumber, monthNumber, 11), data.numberOfOrders[10].saleCount], [gd(yearNumber, monthNumber, 12), data.numberOfOrders[11].saleCount],
                [gd(yearNumber, monthNumber, 13), data.numberOfOrders[12].saleCount], [gd(yearNumber, monthNumber, 14), data.numberOfOrders[13].saleCount], [gd(yearNumber, monthNumber, 15), data.numberOfOrders[14].saleCount], [gd(yearNumber, monthNumber, 16), data.numberOfOrders[15].saleCount],
                [gd(yearNumber, monthNumber, 17), data.numberOfOrders[16].saleCount], [gd(yearNumber, monthNumber, 18), data.numberOfOrders[17].saleCount], [gd(yearNumber, monthNumber, 19), data.numberOfOrders[18].saleCount], [gd(yearNumber, monthNumber, 20), data.numberOfOrders[19].saleCount],
                [gd(yearNumber, monthNumber, 21), data.numberOfOrders[20].saleCount], [gd(yearNumber, monthNumber, 22), data.numberOfOrders[21].saleCount], [gd(yearNumber, monthNumber, 23), data.numberOfOrders[22].saleCount], [gd(yearNumber, monthNumber, 24), data.numberOfOrders[23].saleCount],
                [gd(yearNumber, monthNumber, 25), data.numberOfOrders[24].saleCount], [gd(yearNumber, monthNumber, 26), data.numberOfOrders[25].saleCount], [gd(yearNumber, monthNumber, 27), data.numberOfOrders[26].saleCount], [gd(yearNumber, monthNumber, 28), data.numberOfOrders[27].saleCount],
                [gd(yearNumber, monthNumber, 29), data.numberOfOrders[28]?.saleCount], [gd(yearNumber, monthNumber, 30), data.numberOfOrders[29]?.saleCount], [gd(yearNumber, monthNumber, 31), data.numberOfOrders[30]?.saleCount]
            ];
        }
    });


    //var data3 = [
    //    [gd(2020, 1, 1), 7], [gd(2020, 1, 2), 6], [gd(2020, 1, 3), 4], [gd(2020, 1, 4), 8],
    //    [gd(2020, 1, 5), 9], [gd(2020, 1, 6), 7], [gd(2020, 1, 7), 5], [gd(2020, 1, 8), 4],
    //    [gd(2020, 1, 9), 7], [gd(2020, 1, 10), 8], [gd(2020, 1, 11), 9], [gd(2020, 1, 12), 6],
    //    [gd(2020, 1, 13), 4], [gd(2020, 1, 14), 5], [gd(2020, 1, 15), 11], [gd(2020, 1, 16), 8],
    //    [gd(2020, 1, 17), 8], [gd(2020, 1, 18), 11], [gd(2020, 1, 19), 11], [gd(2020, 1, 20), 6],
    //    [gd(2020, 1, 21), 6], [gd(2020, 1, 22), 8], [gd(2020, 1, 23), 11], [gd(2020, 1, 24), 13],
    //    [gd(2020, 1, 25), 7], [gd(2020, 1, 26), 9], [gd(2020, 1, 27), 9], [gd(2020, 1, 28), 8],
    //    [gd(2020, 1, 29), 5], [gd(2020, 1, 30), 8], [gd(2020, 1, 31), 25]
    //];

    var dataset = [
        {
            label: "Количество продаж",
            data: data2,
            yaxis: 2,
            color: "#464f88",
            lines: {
                lineWidth: 1,
                show: true,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            },
            splines: {
                show: false,
                tension: 0.6,
                lineWidth: 1,
                fill: 0.1
            },
        }
    ];


    var options = {
        xaxis: {
            mode: "time",
            tickSize: [1, "day"],
            tickLength: 0,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 10,
            color: "#d5d5d5"
        },
        yaxes: [{
            position: "left",
            max: 1070,
            color: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 3
        }, {
            position: "right",
            clolor: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: ' Arial',
            axisLabelPadding: 67
        }
        ],
        legend: {
            noColumns: 1,
            labelBoxBorderColor: "#000000",
            position: "nw"
        },
        grid: {
            hoverable: false,
            borderWidth: 0
        }
    };

    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
    }

    $.plot($("#flot-dashboard-chart"), dataset, options);

});