﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DariShop.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DariShop.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly AppDbContext _context;
        public List<Product> Products { get; set; }

        public IndexModel(ILogger<IndexModel> logger, AppDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public void OnGet()
        {
            Products = _context.Products.ToList();
        }


        //Обработчик для удаления товара
        public async Task<IActionResult> OnGetDelete(int id)
        {
            var product = _context.Products.SingleOrDefault(x => x.Id == id);

            _context.Products.Remove(product);
            _context.SaveChanges();
            return RedirectToPage("/Index");
        }

        public void OnPost(string searchParam)
        {
            if (string.IsNullOrEmpty(searchParam))
            {
                Products = _context.Products.ToList();
            }
            else
            {
                Products = _context.Products.Where(x => x.Name.Contains(searchParam)).ToList();
            }
        }
    }
}
