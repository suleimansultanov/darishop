﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DariShop.Entities;
using DariShop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DariShop.Pages
{
    [Authorize]
    public class CreateProductModel : PageModel
    {
        IHostingEnvironment _appEnvironment;
        private AppDbContext _context;

        //Dependency Injection - внедрение зависимостей
        public CreateProductModel(IHostingEnvironment appEnvironment, AppDbContext context)
        {
            _appEnvironment = appEnvironment;
            _context = context;
        }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost(ProductModel model)
        {
            string path = Path.Combine(_appEnvironment.WebRootPath, "Images");
            
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            // загрузка файлов в папку wwwroot
            path = Path.Combine(path, model.Image.FileName);
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await model.Image.CopyToAsync(fileStream);
            }

            var rootImageUrl = "http://127.0.0.1:3131";
            var imageUrl = Path.Combine(rootImageUrl, model.Image.FileName);
            var newProduct = new Product
            {
                Name = model.Name,
                Price = model.Price,
                MainCost = model.MainPrice,
                Count = model.Count,
                ImagePath = imageUrl,
                CreateDate = DateTime.Now
            };

            _context.Products.Add(newProduct);
            _context.SaveChanges();

            return Redirect("/Index");
        }
    }
}
