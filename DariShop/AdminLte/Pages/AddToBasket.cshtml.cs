using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DariShop.Entities;
using DariShop.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace DariShop.Pages
{
    [Authorize]
    public class AddToBasketModel : PageModel
    {
        private AppDbContext _context;
        public List<BasketViewModel> _basket;
        public int _priceSum;
        public AddToBasketModel(AppDbContext context)
        {
            _context = context;
        }
        public void OnGet()
        {
            _basket = _context.ProductBasket
                .Include(x => x.Product)
                .Select(x => new BasketViewModel
                {
                    ProductName = x.ProductName,
                    Count = x.Count,
                    Price = x.Product.Price,
                    Id = x.Id
                }).ToList();

          //  _basket = _context.ProductBasket.Include(x=>x.Product).ToList();

            _priceSum = _basket.Sum(x => x.Price * x.Count);
        }

        public async Task<IActionResult> OnGetAddToBasket(int productId)
        {
            var product = _context.Products.SingleOrDefault(x => x.Id == productId);

            var productExistInBasket = _context.ProductBasket.SingleOrDefault(x => x.ProductId == productId);
            if (productExistInBasket == null)
            {
                var newBasket = new ProductBasket { ProductId = product.Id, ProductName = product.Name, Count = 1};
                _context.ProductBasket.Add(newBasket);
            }
            else
            {
                productExistInBasket.Count++;
            }

            product.Count--;
            _context.Products.Update(product);

            _context.SaveChanges();
            return RedirectToPage("/Index");
        }

        public async Task<IActionResult> OnGetDelete(int id)
        {
            var productBasket = _context.ProductBasket.SingleOrDefault(x => x.Id == id);
            int productsCount = productBasket.Count;
            int productId = productBasket.ProductId;
            _context.ProductBasket.Remove(productBasket);
            var product = _context.Products.SingleOrDefault(x => x.Id == productId);
            if (product != null)
            {
                product.Count += productsCount;
            }
            _context.SaveChanges();
            return RedirectToPage("/AddToBasket");
        }

        public async Task<IActionResult> OnGetPay()
        {
            var allBaskets = _context.ProductBasket.Include(x=>x.Product).ToList();

            foreach (var basket in allBaskets)
            {
                var newHistory = new History()
                {
                    ProductId = basket.ProductId,
                    DateSold = DateTime.Now,
                    Count = basket.Count,
                    ProductName = basket.ProductName,
                    ProductPrice = basket.Product.Price
                };
                _context.Histories.Add(newHistory);
            }

            _context.ProductBasket.RemoveRange(allBaskets);
            _context.SaveChanges();

            return RedirectToPage("/Index");
        }
    }
}
