﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DariShop.Entities;
using DariShop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DariShop.Pages
{
    [Authorize]
    public class EditProductModel : PageModel
    {
        public Product _product;
        private AppDbContext _context;
        private IHostingEnvironment _appEnvironment;
        public EditProductModel(AppDbContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }
        public void OnGetEdit(int id)
        {
            _product = _context.Products.SingleOrDefault(x => x.Id == id);
        }

        public async Task<IActionResult> OnPost(ProductModel model)
        {
            var existProduct = _context.Products.SingleOrDefault(x => x.Id == model.Id);

            existProduct.Name = model.Name;
            existProduct.MainCost = model.MainPrice;
            existProduct.Price = model.Price;
            existProduct.Count = model.Count;

            if (model.Image != null)
            {
                string path = Path.Combine(_appEnvironment.WebRootPath, "Images");

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                // загрузка файлов в папку wwwroot
                path = Path.Combine(path, model.Image.FileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await model.Image.CopyToAsync(fileStream);
                }

                var rootImageUrl = "http://127.0.0.1:3131";
                var imageUrl = Path.Combine(rootImageUrl, model.Image.FileName);
                existProduct.ImagePath = imageUrl;
            }

            _context.Products.Update(existProduct);
            _context.SaveChanges();

            return RedirectToPage("/Index");
        }
    }
}
