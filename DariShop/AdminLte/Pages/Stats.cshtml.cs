using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DariShop.Entities;
using DariShop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace DariShop.Pages
{
    [Authorize]
    public class StatsModel : PageModel
    {
        public List<StatsViewModel> saleStats;
        public List<History> SoldProducts;
        private readonly AppDbContext _context;
        public ChartResponseModel chartResponse;
        public int SoldProductCount;
        public int ProfitSum;
        public StatsModel(AppDbContext context)
        {
            _context = context;
        }
        public void OnGet()
        {
            //saleStats = _context.ProductBasket
            //    .Include(x => x.Product)
            //    .Where(x => x.IsSold)
            //    .Select(x => new StatsViewModel()
            //    {
            //        Profit = x.Product.Price - x.Product.MainCost * x.Count,
            //        DateTime = x.DateSold
            //    }).ToList();
            var now = DateTime.Now;
            SoldProductCount = _context.Histories.Sum(x => x.Count);
            int mainCostSum = _context.Histories.Include(x => x.Product).Sum(x => x.Product.MainCost * x.Count);
            int sum = _context.Histories.Include(x => x.Product).Sum(x => x.Product.Price * x.Count);
            ProfitSum = sum - mainCostSum;

            SoldProducts = _context.Histories
                .Include(x => x.Product)
                .Where(x => x.DateSold.Month == now.Month && x.DateSold.Year == now.Year)
                .GroupBy(x => x.ProductName)
                .Select(cl => new History
                {
                    ProductName = cl.Key,
                    DateSold = cl.Max(x=>x.DateSold),
                    Count = cl.Sum(x => x.Count)
                })
                .Take(10)
                .ToList();
        }

        public JsonResult OnGetGetInfo()
        {

            var dateTimeNow = DateTime.Now;
            int currentMonth = dateTimeNow.Month;
            int currentYear = dateTimeNow.Year;

            var firstDayOfMonth = new DateTime(dateTimeNow.Year, dateTimeNow.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            var infoForCurrentMonth = _context.Histories
                .Include(x => x.Product)
                .Where(x => x.DateSold.Month == currentMonth && x.DateSold.Year == currentYear)
                .ToList();


            var chartDto = new List<Chart>();
            for (int i = 0; i < lastDayOfMonth.Day; i++)
            {
                var info = new Chart()
                {
                    DayNumber = i + 1
                };
                var date = new DateTime(currentYear, currentMonth, i + 1);
                info.SaleCount = infoForCurrentMonth.Count(x => x.DateSold.Date == date.Date);
                chartDto.Add(info);
            }

            chartResponse = new ChartResponseModel()
            {
                yearNumber = currentYear,
                monthNumber = currentMonth,
                numberOfOrders = chartDto
            };

            return new JsonResult(chartResponse);
        }

    }
}
