using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DariShop.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DariShop.Pages
{
    [Authorize]
    public class RegistrationRequestModel : PageModel
    {
        public RegistrationRequestModel(AppDbContext context)
        {
            _context = context;
        }

        private readonly AppDbContext _context;
        public List<RegistrationRequest> RegistrationRequests { get; set; }
        public void OnGet()
        {
            RegistrationRequests = _context.RegistrationRequests.ToList();
        }
    }
}
