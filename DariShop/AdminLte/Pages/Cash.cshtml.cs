using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DariShop.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace DariShop.Pages
{
    [Authorize]
    public class CashModel : PageModel
    {
        public List<History> _basketHistory;
        public int Sum;
        public int Count;
        public int Profit;
        public int CustomerSalary;
        public CashModel(AppDbContext context)
        {
            _context = context;
        }

        private readonly AppDbContext _context;
        public void OnGet()
        {
            _basketHistory = _context.Histories
                .Include(x => x.Product)
                .GroupBy(x=>x.ProductName)
                .Select(cl => new History
                {
                    ProductName = cl.Key,
                    ProductPrice = cl.Sum(x=>x.ProductPrice*x.Count),
                    Count = cl.Sum(x=>x.Count)
                }).ToList();

            int mainCostSum = _context.Histories.Include(x => x.Product).Sum(x => x.Product.MainCost * x.Count);
            Sum = _context.Histories.Include(x => x.Product).Sum(x => x.Product.Price * x.Count);
            Count = _context.Histories.Include(x => x.Product).Sum(x => x.Count);
            Profit = Sum - mainCostSum;
            CustomerSalary = (Sum / 100) * 5;
        }
    }
}
